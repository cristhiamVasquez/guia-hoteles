////// GRUNT ////////

//Instalar Grunt para que funcione en windows
npm i grunt-cli -g --save

//Instalar todos los paquetes de Grunt para automatizar tareas.
npm i grunt grunt-browser-sync grunt-contrib-clean grunt-contrib-concat grunt-contrib-copy grunt-contrib-imagemin grunt-contrib-sass grunt-contrib-uglify grunt-contrib-watch grunt-filerev grunt-usemin grunt-contrib-cssmin --save-dev

//Instalar 1 a 1 cada componente grunt
npm i grunt --save-dev
//Complemetos plugin
npm i grunt-browser-sync --save-dev
npm i grunt-contrib-clean --save-dev
npm i grunt-contrib-concat --save-dev
npm i grunt-contrib-copy --save-dev
npm i grunt-contrib-imagemin --save-dev
npm i grunt-contrib-sass --save-dev
npm i grunt-contrib-uglify --save-dev
npm i grunt-contrib-watch --save-dev
npm i grunt-filerev --save-dev
npm i grunt-usemin --save-dev
npm i grunt-contrib-cssmin --save-dev





////// GULP ///////
Instalador del cliente gulp
npm i gulp -g gulp-cli

Complementos iniciales para levantar el protecto y aplicar cambios en sass
npm i gulp --save-dev
npm i gulp-sass --save-dev
npm i browser-sync --save-dev

//Instalar todos los paquetes de gulp para levantar el proyecto.
npm i gulp gulp-sass browser-sync  --save-dev

//Complementos para minificar el proyecto
npm i del --save-dev
npm i gulp-imagemin --save-dev
npm i gulp-uglify --save-dev
npm i gulp-usemin --save-dev
npm i gulp-rev --save-dev
npm i gulp-clean-css --save-dev
npm i gulp-flatmap --save-dev
npm i gulp-htmlmin --save-dev

//Instalar todos los paquetes de gulp para minificar el proyecto.
npm i del gulp-imagemin gulp-uglify gulp-usemin gulp-rev gulp-clean-css gulp-flatmap gulp-htmlmin --save-dev